import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestContext;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;
@Listeners(TestListner.class)
public class DemoTest {
    private static WebDriver driver;

    @BeforeMethod
    public void Initialize(ITestContext context){
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        context.setAttribute("WebDriver",driver);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
    @DataProvider(name = "user-data")
    public Object[][] data(){
        return new Object[][] {
                {"himaheggade@gmail.com", "Heggade123@!"},
                {"test", "test"}
        };
    }
    @Test(dataProvider = "user-data")
    public void userLogin(String userName, String password) {
        //define the url
        driver.get("http://practice.automationtesting.in/");
        //maximize the window


        String pageTitle = driver.getTitle();
        System.out.println("The title of this page is ===> " +pageTitle);
        driver.findElement(By.linkText("My Account")).click();
        driver.findElement(By.id("username")).sendKeys(userName);
        driver.findElement(By.id("password")).sendKeys(password);
        driver.findElement(By.name("login")).click();
        driver.findElement(By.xpath("//*[@id=\"page-36\"]/div/div[1]/nav/ul/li[2]/a")).click();

    }

    @AfterMethod
    public static void tearDown() {
        driver.quit();
    }

}
